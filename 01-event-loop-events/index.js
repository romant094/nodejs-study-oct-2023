const fs = require('fs')
const dns = require('dns')

const info = text => {
  console.log(text, performance.now().toFixed(2))
}

console.log('Program start')

// Close
fs.writeFile('./test.txt', 'Hello Node.js', () => info('File written'))

//Promises
Promise.resolve().then(() => info('Promise 1'))

// nextTick
process.nextTick(() => info('Next tick 1'))

// setImmediate (Check)
setImmediate(() => info('Immediate 1'))

// Timeouts
setTimeout(() => info('Timeout 1'), 0)
setTimeout(() => {
  process.nextTick(() => info('Next tick 2'))
  info('Timeout 2')
}, 100)

// Intervals
let intervalCount = 1
const interval = setInterval(() => {
  info(`Interval ${intervalCount++}`)
  if (intervalCount > 2) {
    clearInterval(interval)
  }
}, 50)


// I/O events
dns.lookup('localhost', (err, address, family) => {
  info('DNS 1 localhost')
  Promise.resolve().then(() => info('Promise 2'))
  process.nextTick(() => info('Next tick 3'))
})

console.log('Program end')