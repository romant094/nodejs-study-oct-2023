// node create-file.mjs <filename> <linesQty>
import fs from 'fs'
import path from 'path'

const [_, __, fileName, linesQtyString] = process.argv

if (!fileName || !linesQtyString) {
  console.log('File name and lines quantity must be set')
  process.exit(0)
}

const linesQty = parseInt(linesQtyString)

if (isNaN(linesQty)) {
  console.log('Lines qty must be a number')
  process.exit(0)
}

const writeStream = fs.createWriteStream(path.join('./files', fileName))
// blocking for
for (let i = 1; i <= linesQty; i++) {
  writeStream.write(`This is a line ${i}\n`)
}

writeStream.end(() => {
  console.log(`File ${fileName} was created`)
})