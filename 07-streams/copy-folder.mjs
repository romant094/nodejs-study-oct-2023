import fs from 'fs'
import path from 'path'

const sourceDir = './files'
const destinationDir = './copied-files'

if (!fs.existsSync(sourceDir)) {
  console.log(`${sourceDir} does not exist`)
  process.exit(0)
}

if (fs.existsSync(destinationDir)) {
  fs.rmSync(destinationDir, { recursive: true })
}

fs.mkdirSync(destinationDir)
fs.readdir(sourceDir, (err, fileNames) => {
  if (err) {
    console.log(err)
    process.exit(1)
  }

  fileNames.forEach((fileName, index) => {
    const sourcePath = path.join(sourceDir, fileName)
    const destPath = path.join(destinationDir, `${index + 1}-${fileName}`)

    const readStream = fs.createReadStream(sourcePath)
    const writeStream = fs.createWriteStream(destPath)

    readStream.pipe(writeStream)
    writeStream.on('finish', () => console.log(`File ${fileName} was copied.`))
  })

  // for (let i=0; i<fileNames.length;i++) {
  //   const readStream = fs.createReadStream(`${sourceDir}/${fileNames[i]}`)
  //   const writeStream = fs.createWriteStream(`${destinationDir}/copied-${fileNames[i]}`)
  //
  //   readStream.pipe(writeStream)
  //   writeStream.on('finish', () => console.log(`File ${fileNames[i]} was copied.`))
  // }
})