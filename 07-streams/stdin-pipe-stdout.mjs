import { Transform } from 'stream'
import fs from 'fs'

const filePath = './files/stdin-dump.txt'

const upperCaseStream = new Transform({
  transform (chunk, encoding, callback) {
    // chunk is a Buffer here; so it is needed to be converted to a string
    const upperCased = chunk.toString().toUpperCase()
    callback(null, upperCased)
  }
})

const revertStream = new Transform({
  transform (chunk, encoding, callback) {
    const arrayOfChars = chunk.toString().split('')
    const lastChar = arrayOfChars.pop()
    const reverted = arrayOfChars.reverse().concat(lastChar).join('')
    callback(null, reverted)
  }
})

const writableStream = fs.createWriteStream(filePath, 'utf-8')

process.stdin
  .pipe(upperCaseStream)
  .pipe(revertStream)
  .pipe(writableStream)

// process.stdin.pipe(process.stdout)