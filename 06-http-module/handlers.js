const fs = require('fs')
const qs = require('querystring')
const { comments } = require('./data')

const getHTML = (req, res) => {
  res.statusCode = 200
  res.setHeader('Content-Type', 'text/html')
  res.write('<html lang="en"><body><div>')
  res.write('<h1>Hello from http Node.js server</h1>')
  res.write('</div></body></html>')
  return res.end()
}

const getText = (req, res) => {
  res.statusCode = 200
  res.setHeader('Content-Type', 'text/plain')
  res.end('This is a plain text')
}

const getComments = (req, res) => {
  res.statusCode = 200
  res.setHeader('Content-Type', 'application/json')
  res.end(JSON.stringify(comments))
}

const handleNotFound = (req, res) => {
  res.statusCode = 404
  res.setHeader('Content-Type', 'text/html')
  res.write('<html lang="en"><body><div>')
  res.write(`<h1>Page not found...</h1><p>Current page: <strong>${req.url}</strong></p>`)
  res.write('</div></body></html>')
  res.end()
}

const postComment = (req, res) => {
  res.setHeader('Content-Type', 'text/plain')

  if (req.headers['content-type'] === 'application/x-www-form-urlencoded') {
    try {
      let body = ''
      req.on('data', chunk => body += chunk)
      req.on('end', () => {
        const comment = qs.parse(body)
        comments.push(comment)
        res.statusCode = 200
        // res.end('Comment created')
        // res.redirect('/comments')
        res.writeHead(302, {
          'Location': '/comments'
        });
        res.end();
      })
    } catch (err) {
      res.statusCode = 400
      res.end('Invalid form data')
    }
  } else if (req.headers['content-type'] === 'application/json') {
    let comment = ''

    req.on('data', chunk => comment += chunk)

    req.on('end', () => {
      try {
        comments.push(JSON.parse(comment))
        res.statusCode = 200
        res.end('Comment created')
      } catch (err) {
        res.statusCode = 400
        res.end('Invalid JSON')
      }
    })
  } else {
    res.statusCode = 400
    res.end('Content should be in JSON format')
  }
}

const getMainPage = (req, res) => {
  fs.readFile('./files/comment-form.html', (err, data) => {
    if (err) {
      res.statusCode = 500
      res.setHeader('Content-Type', 'text/plain]')
      return res.end('Error while reading HTML file')
    }
    res.statusCode = 200
    res.setHeader('Content-Type', 'text/html')
    res.end(data)
  })
}

module.exports = {
  getHTML,
  getComments,
  getText,
  handleNotFound,
  postComment,
  getMainPage
}