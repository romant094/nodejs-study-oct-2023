const comments = [
  { id: 1, text: 'Text example 1', name: 'Name 1' },
  { id: 2, text: 'Text example 2', name: 'Name 2' },
  { id: 3, text: 'Text example 3', name: 'Name 3' }
]

module.exports = {
  comments
}