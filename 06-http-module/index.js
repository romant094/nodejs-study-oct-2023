const http = require('http')
const {
  getText,
  getComments,
  getHTML,
  postComment,
  handleNotFound,
  getMainPage
} = require('./handlers')

const PORT = 3000

const queries = [
  { url: '/html', method: 'GET', response: getHTML },
  { url: '/text', method: 'GET', response: getText },
  { url: '/comments', method: 'GET', response: getComments },
  { url: '/comments', method: 'POST', response: postComment },
  { url: '/', method: 'GET', response: getMainPage },
]

const server = http.createServer((req, res) => {
  const { response } = queries.find(({ url, method }) => {
    return url === req.url && method === req.method
  }) || {}
  return response ? response(req, res) : handleNotFound(req, res)
})

server.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`)
})