const fs = require('fs')

const fileName = 'third.txt'

try {
  fs.writeFileSync(`./${fileName}`, 'Hello world')
  console.log(`File ${fileName} created`)
  fs.appendFileSync(`./${fileName}`, '\nOne more line')
  console.log(`File ${fileName} appended`)
  fs.renameSync(`./${fileName}`, `renamed-${fileName}`)
  console.log(`File ${fileName} was renamed`)
} catch (err) {
  console.log(err)
}