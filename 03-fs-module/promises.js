const fs = require('fs/promises')

const fileName = 'second.txt'

fs.writeFile(`./${fileName}`, 'Hello world')
  .then(() => console.log(`File ${fileName} created`))
  .then(() => fs.appendFile(`./${fileName}`, '\nOne more line'))
  .then(() => console.log(`File ${fileName} appended`))
  .then(() => fs.rename(`./${fileName}`, `renamed-${fileName}`))
  .then(() => console.log(`File ${fileName} was renamed`))
  .catch(console.log)