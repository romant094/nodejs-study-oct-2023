const fs = require('fs')

const fileName = 'first.txt'
fs.writeFile(`./${fileName}`, 'Hello world', (err) => {
  if (err) {
    console.log(err)
  } else {
    console.log(`File ${fileName} created`)
    fs.appendFile(`./${fileName}`, '\nOne more line', err => {
      if (err) {
        console.log(err)
      } else {
        console.log(`File ${fileName} appended`)
        fs.rename(`./${fileName}`, `renamed-${fileName}`, err => {
          if (err) {
            console.log(err)
          } else {
            console.log(`File ${fileName} was renamed`)
          }
        })
      }
    })
  }
})

