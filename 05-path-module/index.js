const path = require('path')

const filePath = '/Users/username/node/index.js'
const textFilePath = '/Users/username/Desktop/file.txt'
const relativeFilePath = './node/file.mov'
const directoryPath = './node/subfolder'

console.log(path.isAbsolute(filePath)) // true
console.log(path.isAbsolute(relativeFilePath)) // false

console.log(path.basename(filePath)) // index.js
console.log(path.basename(directoryPath)) // subfolder

console.log(path.dirname(filePath)) // /Users/username/node
console.log(path.dirname(directoryPath)) // ./node

console.log(path.resolve(relativeFilePath)) // /Users/username/Projects/studying/node-js/05-path-module/node/file.mov

console.log(path.extname(textFilePath)) // .txt
console.log(path.extname(directoryPath)) // ''

const parsedPath = path.parse(filePath)
console.log(parsedPath)
const newPath = path.join(parsedPath.dir, `renamed-${parsedPath.base}`)
console.log(filePath)
console.log(newPath)
