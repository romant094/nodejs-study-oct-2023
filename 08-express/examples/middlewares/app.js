const express = require('express')
const morgan = require('morgan')
// const qs = require('querystring')

const app = express()

app.use(express.json())

// app.use((req, res, next) => {
//   let data = ''
//   req.on('data', chunk => data += chunk)
//   req.on('end', () => {
//     req.body = JSON.parse(data)
//     next()
//   })
// })

app.use(express.urlencoded({ extended: true }))

// app.use((req, res, next) => {
//   if (req.headers['content-type'] === 'application/x-www-form-urlencoded') {
//     let data = ''
//     req.on('data', chunk => data += chunk.toString())
//     req.on('end', () => {
//       req.body = qs.parse(data)
//       next()
//     })
//   } else {
//     next()
//   }
// })

app.use(morgan('combined'))

app.use((req, res) => {
  console.log(req.body)
  res.send('Express server is working')
})

app.listen(3000, () => {
  console.log('Server is running at port 3000')
})