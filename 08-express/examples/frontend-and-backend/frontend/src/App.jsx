import { useState, useEffect } from 'react'
import './App.css'

function App () {
  const [user, setUser] = useState(null)

  useEffect(() => {
    fetch('http://localhost:3000')
      .then(res => res.json())
      .then(setUser)
      .catch(console.error)
  }, [])

  return (
    <div>
      {user && (
        <>
          <div>User name: {user.name}</div>
          <div>User birth year: {user.birthYear}</div>
        </>
      )}
    </div>
  )
}

export default App
