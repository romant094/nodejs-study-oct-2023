const express = require('express')
const morgan = require('morgan')
const cors = require('cors')

const app = express()

app.use(cors({origin: 'http://localhost:3001'}))
app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(morgan('combined'))

app.use((req, res) => {
  console.log(req.body)
  return res.json({
    name: 'Tony',
    birthYear: 1990
  })
})

app.listen(3000, () => {
  console.log('Server is running at port 3000')
})