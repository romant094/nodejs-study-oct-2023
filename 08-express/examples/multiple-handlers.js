const express = require('express')

const app = express()

const firstHandler = (req, res, next) => {
  console.log('First handler')
  // next() is important to be called to move to the next handler
  next()
}

const secondHandler = (req, res) => {
  console.log('Second handler')
  res.send('Response from the server')
}

app.get('/', firstHandler, secondHandler)

app.listen(3000, () => {
  console.log('Server is running at port 3000')
})