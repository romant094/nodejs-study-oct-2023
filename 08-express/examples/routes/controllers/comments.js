const getCommentsHandler = (req, res) => res.send('getCommentsHandler')
const postCommentsHandler = (req, res) => res.send('postCommentsHandler')
const getSingleCommentHandler = (req, res) => res.send('getSingleCommentHandler: ' + req.params.commentId)
const deleteCommentHandler = (req, res) => res.send('deleteCommentHandler: ' + req.params.commentId)

module.exports = {
  getCommentsHandler,
  postCommentsHandler,
  getSingleCommentHandler,
  deleteCommentHandler
}