const getUsersHandler = (req, res) => res.send('getUsersHandler')
const postUsersHandler = (req, res) => res.send('postUsersHandler')
const getSingleUserHandler = (req, res) => res.send('getSingleUserHandler: ' + req.params.userId)

module.exports = {
  getUsersHandler,
  postUsersHandler,
  getSingleUserHandler
}