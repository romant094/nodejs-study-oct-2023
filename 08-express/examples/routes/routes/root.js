const { Router } = require('express')
const { getRootHandler } = require('../controllers/root')

const router = Router()

router.get('/', getRootHandler)

module.exports = router