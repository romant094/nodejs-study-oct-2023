const mongoose = require('mongoose')

const schema = new mongoose.Schema({
  name: String,
  price: Number,
  brand: String
}, {
  timestamps: true,
  toObject: {
    transform: (_, ret) => {
      ret.id = ret._id
      delete ret._id
      delete ret.__v
      return ret
    }
  }
})

module.exports = mongoose.model('Product', schema)