const mongoose = require('mongoose')

module.exports = async () => {
  try {
    await mongoose.connect(process.env.DB_URL, { useNewUrlParser: true })
    console.info('Database connected')
  } catch (err) {
    console.log(`Database connection error: ${err}`)
    throw new Error(err)
  }
}