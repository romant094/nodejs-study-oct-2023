const mongoose = require('mongoose')
const constants = require('../constants')
const formatMongoData = data => {
  if (Array.isArray(data)) {
    return data.map(d => d.toObject())
  }
  return data.toObject()
}

const checkObjectId = id => {
  if (!mongoose.isValidObjectId(id)) {
    throw new Error(constants.databaseMessages.INVALID_ID)
  }
}

module.exports = {
  formatMongoData,
  checkObjectId
}