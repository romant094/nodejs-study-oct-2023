const express = require('express')
const cors = require('cors')
const { config } = require('dotenv')
const dbConnect = require('./database/connection')
const productsRouter = require('./routes/products')

config()

const PORT = process.env.PORT || 3000

const app = express()

void dbConnect()

app.use(cors())
app.use(express.json())
app.use(express.urlencoded({extended: true}))

app.use('/api/v1/products', productsRouter)

app.use('/', (req, res) => {
  res.send('Hello from Express API server')
})

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`)
})

// Error-handling middleware
app.use((err, req, res, next) => {
  console.error(err.stack)
  res.status(500).send({
    status: 500,
    message: err.stack,
    body: {}
  })
})