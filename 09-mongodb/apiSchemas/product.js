const Joi = require('joi')

const createProductSchema = Joi.object().keys({
  name: Joi.string().required(),
  brand: Joi.string().required(),
  price: Joi.number().required(),
})

const getAllProductsSchema = Joi.object().keys({
  skip: Joi.string(),
  limit: Joi.string()
})

module.exports = {
  createProductSchema,
  getAllProductsSchema
}