const productsService = require('../service/products')
const constants = require('../constants')
const createProduct = async (req, res) => {
  const response = { ...constants.defaultResponse }

  try {
    const resp = await productsService.createProduct(req.body)
    response.status = 201
    response.message = constants.product.PRODUCT_CREATED
    response.body = resp
  } catch (err) {
    console.log(`Error in createProduct controller: ${err}`)
    response.message = err.message
  }
  res.status(response.status).send(response)
}

const getAllProducts = async (req, res) => {
  const response = { ...constants.defaultResponse }

  try {
    const resp = await productsService.getAllProducts(req.query)
    response.status = 200
    response.message = constants.product.PRODUCTS_FETCHED
    response.body = resp
  } catch (err) {
    console.log(`Error in getAllProducts controller: ${err}`)
    response.message = err.message
  }
  res.status(response.status).send(response)
}

const getProductById = async (req, res) => {
  const response = { ...constants.defaultResponse }
  // console.log(req)
  try {
    const resp = await productsService.getProductById(req.params.id)
    response.status = 200
    response.message = constants.product.PRODUCT_FETCHED
    response.body = resp
  } catch (err) {
    console.log(`Error in getProductById controller: ${err}`)
    response.message = err.message
  }
  res.status(response.status).send(response)
}

module.exports = {
  createProduct,
  getAllProducts,
  getProductById
}