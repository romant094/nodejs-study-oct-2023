module.exports = {
  defaultResponse: {
    status: 400,
    message: '',
    body: {}
  },
  product: {
    PRODUCT_CREATED: 'Product was successfully created',
    PRODUCTS_FETCHED: 'Products fetched successfully',
    PRODUCT_FETCHED: 'Product fetched successfully',
    PRODUCT_NOT_FOUND: 'Product not found'
  },
  validationMessages: {
    BAD_REQUEST: 'Invalid fields'
  },
  databaseMessages: {
    INVALID_ID: 'Invalid id'
  }
}