const mongoose = require('mongoose')
const Product = require('../database/models/product')
const { formatMongoData, checkObjectId } = require('../helpers/dbHelper')
const constants = require('../constants')

const createProduct = async (data) => {
  try {
    const product = new Product(data)
    const result = await product.save()
    return formatMongoData(result)
  } catch (err) {
    console.log(`Error in createProduct service: ${err}`)
    throw new Error(err)
  }
}

const getAllProducts = async ({ skip = '0', limit = '10' }) => {
  try {
    const products = await Product.find({}).skip(parseInt(skip)).limit(parseInt(limit))
    return formatMongoData(products)
  } catch (err) {
    console.log(`Error in getAllProducts service: ${err}`)
    throw new Error(err)
  }
}

const getProductById = async (id) => {
  try {
    checkObjectId(id)
    const product = await Product.findById(id)
    if (!product) {
      throw new Error(constants.product.PRODUCT_NOT_FOUND)
    }
    return formatMongoData(product)
  } catch (err) {
    console.log(`Error in getProductById service: ${err}`)
    throw new Error(err)
  }
}

module.exports = {
  createProduct,
  getAllProducts,
  getProductById
}