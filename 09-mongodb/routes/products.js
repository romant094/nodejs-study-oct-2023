const { Router } = require('express')
const { createProduct, getAllProducts, getProductById } = require('../controller/products')
const { validateBody, validateQueryParams } = require('../middleware/joyValidationSchema')
const { createProductSchema, getAllProductsSchema } = require('../apiSchemas/product')

const router = Router()
router.post('/', validateBody(createProductSchema), createProduct)
router.get('/', validateQueryParams(getAllProductsSchema), getAllProducts)
router.get('/:id', getProductById)

module.exports = router