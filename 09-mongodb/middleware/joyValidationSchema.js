const constants = require('../constants')
const validateData = (data, schema) => {
  const result = schema.validate(data, { convert: false })
  if (result.error) {
    return result.error.details.map(({ path, message }) => ({ path, message }))
  }
  return null
}

const validateBody = schema => (req, res, next) => {
  const response = { ...constants.defaultResponse }
  const err = validateData(req.body, schema)
  if (err) {
    response.body = err
    response.message = constants.validationMessages.BAD_REQUEST
    return res.status(response.status).send(response)
  }
  return next()
}

const validateQueryParams = schema => (req, res, next) => {
  const response = { ...constants.defaultResponse }
  const err = validateData(req.query, schema)
  if (err) {
    response.body = err
    response.message = constants.validationMessages.BAD_REQUEST
    return res.status(response.status).send(response)
  }
  return next()
}

module.exports = {
  validateBody,
  validateQueryParams
}