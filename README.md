# Theory

## Node.js architecture
![img.png](docs-images/architecture.png)

## Event loop (EL)
I/O - input/output  

### EL stages
1. Timers (setTimeout and setInterval callbacks)  
2. Pending (I/O callbacks postponed for the next iteration)  
3. Idle, Prepare (used inside Node.js)  
4. Poll (get and run I/O callbacks)  
5. Check (setImmediate callbacks)  
6. Close (close event callbacks)

EL will run indefinitely when starting the http server. Node.js waits for requests from the client or for new files to appear in a certain directory.

**process.nextTick** and **Promise callbacks** have the highest priority.

![img.png](docs-images/next-tick.png)

### setImmediate
The code inside setImmediate will be executed asynchronously in the next iteration of the EL.

## Modules

### CommonJS modules
Each module is wrapped by anonymous function. So there 5 available globals in each module by default.
```js
(function (exports, require, module, __filename, __dirname) {})
```  
`exports` is an alias to `module.exports`.
```js
const helloFunc = () => console.log('Hello world')

// This is the same
module.exports.helloFunc = helloFunc
exports.helloFunc = helloFunc
```
There is no export if `exports` was reassigned.
```js
// ❌ This is a wrong way of export
exports = () => {
  console.log('Hello world')
}
```
Module can have only one export. It can be done by reassigning module.exports:
```js
module.exports = () => {
  console.log('Hello world')
}
```

## ES6 modules
2 ways to use ES6 modules:
- rename extension to .mjs
- use "type": "module" in package.json

ES6 Modules are not wrapped by anonymous function. As a result there is no access to variables `exports`, `require`, `module`, `__filename` & `__dirname`.

## Streams
Stream is needed to proceed the data by parts - one by one.

Stream types:
- Readable - for reading data
- Writable - for writing data
- Duplex - for reading and writing data
- Transform - duplex stream for transforming data

`pipe` transfers one stream to another.
```js
readableStream.pipe(writableStream)
```
`pipe` returns a new stream, so it can be used multiple times (like a promise).
```js
stream1
  .pipe(stream2)
  .pipe(stream3)

// This is the same like
stream1.pipe(stream2)
stream2.pipe(stream3)
```
When transferring data from initial stream to target finishes an event `close` occurs on the target stream.