let isRunning = true

setTimeout(() => isRunning = false, 10)
process.nextTick(() => console.log('Next tick'))

const setImmediatePromise = async () => new Promise((resolve, reject) => {
  // setImmediate is needed to break current iteration
  setImmediate(() => resolve())
  // with simple resolve() we stay on the current event loop iteration
  // resolve()
})

const whileLoop = async () => {
  while (isRunning) {
    console.log('While loop is running...')
    await setImmediatePromise()
  }
}

whileLoop().then(() => console.log('While loop ended'))