import fs from 'fs'
import EventEmitter from 'events'

const emitter = new EventEmitter()
const fileName = 'example.txt'

emitter.on('writeComplete', () => {
  console.log(`File ${fileName} created`)

  fs.appendFile(`./${fileName}`, '\nOne more line', () => {
    emitter.emit('appendComplete')
  })
})

emitter.on('writeComplete', () => {
  console.log(`File ${fileName} appended`)

  fs.rename(`./${fileName}`, `renamed-${fileName}`, () => {
    emitter.emit('renameComplete')
  })
})

emitter.on('writeComplete', () => {
  console.log(`File ${fileName} was renamed`)
})

fs.writeFile(`./${fileName}`, 'Hello world', () => {
  emitter.emit('writeComplete')
})


