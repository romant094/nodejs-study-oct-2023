import EventEmitter from 'events'

class Post extends EventEmitter{
  constructor (author, text) {
    super()
    this.author = author
    this.text = text
    this.likes = 0

    this.on('likePost', (times, user) => console.log(`${user} liked the post. Total likes: ${times}.`))
    this.on('error', console.error)
  }

  like(username) {
    if (!username) {
      return this.emit('error', new Error('No username in request'))
    }
    this.likes++
    this.emit('likePost', this.likes, username)
  }
}

const post = new Post('Anton', 'Hello NodeJS events')

post.like('Tony Stark')
setTimeout(() => post.like(), 1000)
setTimeout(() => post.like('Spider-man'), 2000)