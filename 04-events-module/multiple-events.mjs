import EventEmitter from 'events'

const emitter = new EventEmitter()

// By default, there are only 10 listeners. But it can be changed
emitter.setMaxListeners(25)
console.log(emitter.getMaxListeners())
emitter.on('myEvent', () => console.log('First event'))
emitter.on('myEvent', () => console.log('Second event'))
emitter.on('otherEvent', () => console.log('Other event'))

emitter.emit('myEvent')
emitter.emit('otherEvent')

// To check all events on the emitter
console.log(emitter.eventNames())
