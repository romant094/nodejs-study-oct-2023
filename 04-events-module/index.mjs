import EventEmitter from 'events'

const timeoutListener = (secondsQty) => {
  console.log(`Timeout event in ${secondsQty}`)
}

const emitter = new EventEmitter()

emitter.on('timeout', timeoutListener)

setTimeout(() => emitter.emit('timeout', 1), 1000)
setTimeout(() => emitter.emit('timeout', 2), 2000)

emitter.once('singleEvent', () => {
  console.log('Single event occurred')
})

// Listener will be called only once
setTimeout(() => emitter.emit('singleEvent'), 1000)
setTimeout(() => emitter.emit('singleEvent'), 2000)

// Remove listener
setTimeout(() => {
  emitter.off('timeout', timeoutListener)
}, 3000)

// Listener will not be called
setTimeout(() => emitter.emit('timeout', 4), 4000)